# Halley OpenScad 001

Se incluyen dos piezas en el mismo archivo, la caja (enclosure) y la tapa (cover).
Al inicio del documento se pueden ver las dos funciones que generan ambas partes:

```
enclosure();
cover();
```

En el código se desarrollan ambas funciones con comentarios para entender cada parte.

# Download

* Descarga un zip desde los [Tags del repositorio](https://gitlab.com/rhombio/3dboxes/halley-os-001/-/tags)
* O clona el repositorio: `git clone https://gitlab.com/rhombio/3dboxes/halley-os-001.git`

Diseñado con [OpenScad](https://www.openscad.org/), lo puedes bajar gratis desde su web.

