/**
 * Caja Halleybox con espacio para batería en socket2
 *
 * Esta caja tiene un tamaño ajustado que obliga a montar batería
 * utilizando el espacio en socket 2 (o 1)
 *
 * El código se ha divido en dos partes, el enclosure, que viene a ser
 * la caja en sí misma, y la tapa. Además existen unos tamaños base
 * x, y, z compartidas por ambos (enclosure y tapa)
 *
 * Cada una de las partes tiene una función propia que contiene
 * su código, enclosure() y cover().
 */

// Tamaño base usando como referencia los datos de Mechanical 
// Specifications en los docs de Halley
WIDTH = 51.5; // x
HEIGHT = 12; // y
LEN = 52; // z

$fn = 50; // resolución de polígonos, menor número = menor calidad

// Comenta uno de los dos métodos para mostrar solo una parte
enclosure();
cover();

//################################################################
//                          ENCLOSURE 
// Al usar difference(), y una vez creamos el primer cubo (@cube1),
// los siguientes cubos crearán agujeros en @cube1 según la
// diferencia del tamaño de ambos
//################################################################

module enclosure () {

  difference() {
    // Caja con bordes redondeados en la caja.
    // Alternativamente utilizar solo "cube()" +2:
    // cube([WIDTH + 2, HEIGHT + 2, LEN + 2], center = true);
    // Los 2mm generan el ancho de pared suficiente para crear
    // el reborde de la tapa
    // @name cube1
    minkowski() {
      cube([WIDTH, HEIGHT, LEN], center = true);
      sphere(2); // 2mm
    }
    
    // Reborde superior para soportar la tapa, 1mm de altura
    // @name cube2
    translate([0, HEIGHT, 0])
      cube([WIDTH + 2, HEIGHT, LEN +1], center = true);
    
    // Agujero dentro de la caja (por debajo del reborde-tapa (cube2)
    // @cube3
    minkowski() {
      cube([WIDTH - 2, HEIGHT, LEN - 3], center = true);
      sphere(1);
    }
    
    // usb
    // @name cube4
    translate([-4, 2, -27])
      cube([9, 4, 5], center = true);

    // btn reset
    // @name cube5
    translate([-13, 2, -27])
      cube([3, 3, 5], center = true);
    
    // btn nmi
    // @name cube6 
    translate([-19, 2, -27])
      cube([3, 3, 5], center = true);
  }
  
  // soportes, los sacamos del difference() para que sean visibles
  screws(10); // 5 mm de altura
  
  module screws (height) {
    screw1 = [21, 20, 0];
    screw2 = [-21, 20, 0];
    screw3 = [-21, -20, 0];
    screw4 = [21, -20, 0];
    
    offset= [-15, 5, 0];
    
    screw(screw1);
    screw(screw2);
    screw(screw3);
    screw(screw4);
  }

  module screw(post) {
    rotate([90, 0, 0])
     translate(post)
        difference() {
          cylinder(r=2.5, h = 6);
          cylinder(r=1.5, h = 4);
        }
  }
  
} // fin enclosure()


//################################################################
//                          TAPA / COVER 
// En este caso con difference() logramos crear los agujeros
// en la tapa para los tornillos.
//################################################################

module cover() {
  // Aleja o acerca la tapa para ver como se acopla con el enclosure
  // si se oculta el enclosure() debería tener un valor de 0
  coverDistance = 15;
  
  difference() {
    minkowski() {
      translate([0, coverDistance, 0])
        // el tamaño de la tapa tiene que coincidir con el reborde
        // hecho por enclosure()->@cube2
        cube([WIDTH + 2, 2, LEN + 1], center = true);
    }
    
    screws(20, -1 - coverDistance);
  }

  module screws (height, z) {
    post1 = [21, 20, z];
    post2 = [-21, 20, z];
    post3 = [-21, -20, z];
    post4 = [21, -20, z];
    
    offset= [-15, 5, 0];
    
    screw2(post1, height);
    screw2(post2, height);
    screw2(post3, height);
    screw2(post4, height);
  }

  module screw2(post, height) {
    rotate([90, 0, 0])
      translate(post)
        cylinder(r=1.5, h = height);
  }
}
